DROP TABLE user IF EXISTS;

CREATE TABLE user (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  username VARCHAR (50) NOT NULL UNIQUE,
  email VARCHAR (50) NOT NULL UNIQUE,
  password VARCHAR(100) NOT NULL,
  firstname VARCHAR (50) NOT NULL,
  lastname VARCHAR (50) NOT NULL,
  city VARCHAR(100),
  role VARCHAR(50),
  createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);


INSERT INTO user (id, username, email, password, firstname, lastname, city, role) VALUES (1, 'Test', 'test@test.com', '$2a$10$mSgQaVa94oylqvxZdeaYWObksaiCoJ6MU2Xw3JeGud.lMX5vKJszG', 'test', 'Testy', 'Lyon', 'ROLE_USER');
