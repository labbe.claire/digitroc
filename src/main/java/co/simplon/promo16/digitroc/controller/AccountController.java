package co.simplon.promo16.digitroc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import co.simplon.promo16.digitroc.entity.User;
import co.simplon.promo16.digitroc.repository.DvdRepository;
import co.simplon.promo16.digitroc.repository.UserRepository;

@Controller
public class AccountController {
    
@Autowired
private UserRepository repo;

@Autowired
    private DvdRepository dvdrepo;


    @GetMapping("/profil")
        public String showAccount(Authentication authentication, Model model) {
            User user = (User)authentication.getPrincipal();
            model.addAttribute("user", user);
            model.addAttribute("owndvds", dvdrepo.findByOwner(user.getId()));
        return "profil";
    }

    @GetMapping("/edit")
        public String editAccount(Authentication authentication, Model model) {
            User user = (User)authentication.getPrincipal();
            model.addAttribute("user", user);
        return "edit";
    }

    @PostMapping("/edit")
    public String updateUser(Authentication authentication, User user){
        User actualUser = (User)authentication.getPrincipal();
        
        actualUser.setUsername(user.getUsername());
        actualUser.setEmail(user.getEmail());
        actualUser.setPassword(user.getPassword());
        actualUser.setFirstname(user.getFirstname());
        actualUser.setLastname(user.getLastname());
        actualUser.setCity(user.getCity());
        repo.update(actualUser);

        return "redirect:/profil";
    }

}


