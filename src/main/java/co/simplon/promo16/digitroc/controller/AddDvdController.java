package co.simplon.promo16.digitroc.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import co.simplon.promo16.digitroc.entity.Dvd;
import co.simplon.promo16.digitroc.entity.User;
import co.simplon.promo16.digitroc.repository.CategoryRepository;
import co.simplon.promo16.digitroc.repository.DvdRepository;
import co.simplon.promo16.digitroc.service.Uploader;

@Controller
public class AddDvdController {

    @Autowired
    private DvdRepository dvdrepo;

    @Autowired
    private CategoryRepository catrepo;

    @Autowired
    private Uploader uploader;

    @GetMapping("/add-dvd")
    private String showAddForm(Model model) {
        model.addAttribute("dvd", new Dvd());
        model.addAttribute("category", catrepo.findAll());
        return "add-dvd";
    }

    @PostMapping("/add-dvd")
    public String processAdd(Dvd dvd, @RequestParam("file") MultipartFile file, Authentication authentication) {
        try {
            dvd.setImage(uploader.upload(file));
            User user = (User) authentication.getPrincipal();
            dvd.setUser(user);
            dvdrepo.save(dvd);
        } catch (IOException e) {

            e.printStackTrace();
        }
        return "redirect:/";

    }
}
