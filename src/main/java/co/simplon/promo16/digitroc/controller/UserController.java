package co.simplon.promo16.digitroc.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import co.simplon.promo16.digitroc.entity.User;
import co.simplon.promo16.digitroc.repository.UserRepository;

@Controller
public class UserController {
    
    @Autowired
    private UserRepository repo;

    @Autowired
    private PasswordEncoder encoder;

    @GetMapping("/register")
    public String showRegister(Model model) {
        model.addAttribute("user", new User());

        
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid User user, BindingResult result, Model model) {
        if (result.hasErrors()){

            return "register";
        }
        if(repo.findByEmail(user.getEmail()) != null) {
            model.addAttribute("feedback", "User already exists");
            
            return "register";
        }
        String hashedPassword = encoder.encode(user.getPassword());
        user.setPassword(hashedPassword);
        user.setRole("ROLE_USER");
        repo.save(user);
        return "redirect:/";
    }

}
