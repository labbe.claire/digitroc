package co.simplon.promo16.digitroc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import co.simplon.promo16.digitroc.entity.Barter;
import co.simplon.promo16.digitroc.entity.User;
import co.simplon.promo16.digitroc.repository.BarterRepository;
import co.simplon.promo16.digitroc.repository.DvdRepository;

@Controller
public class AddBarterController {

    @Autowired
    private BarterRepository brepo;

    @Autowired
    private DvdRepository dvdrepo;

    @GetMapping("/add-barter/{id}")
    private String showBarterDemand(Model model, Authentication authentication, @PathVariable Integer id) {
        User actualUser = (User)authentication.getPrincipal();
        model.addAttribute("barter", new Barter());
        model.addAttribute("actualUser", actualUser);
        model.addAttribute("dvd", dvdrepo.findById(id));
        return "add-barter";
    }

    @PostMapping("/add-barter/{id}")
    public String processNewBarter(Barter barter, Authentication authentication, @PathVariable Integer id) {
        User actualUser = (User)authentication.getPrincipal();
        barter.setDemandUser(actualUser);
        barter.setDemandedDvd(dvdrepo.findById(id));
        brepo.save(barter);
        return "redirect:/";

    }

    
}
