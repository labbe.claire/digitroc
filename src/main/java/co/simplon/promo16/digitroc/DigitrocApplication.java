package co.simplon.promo16.digitroc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DigitrocApplication {

	public static void main(String[] args) {
		SpringApplication.run(DigitrocApplication.class, args);
	}

}
