package co.simplon.promo16.digitroc.entity;

import java.sql.Timestamp;


public class Dvd {
    
    private Integer id;
    private String title;
    private int year;
    private String dvdCondition;
    private Long barCode;
    private String description;
    private String image;
    private Timestamp addedAt;
    private User user;
    private Category category;

 

    public Dvd(Integer id, String title, int year, String dvdCondition, Long barCode, String description, String image,
            Timestamp addedAt, User user, Category category) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.dvdCondition = dvdCondition;
        this.barCode = barCode;
        this.description = description;
        this.image = image;
        this.addedAt = addedAt;
        this.user = user;
        this.category = category;
    }

    public Dvd(Integer id, String title, int year, String dvdCondition, Long barCode, String description, String image,
            Timestamp addedAt) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.dvdCondition = dvdCondition;
        this.barCode = barCode;
        this.description = description;
        this.image = image;
        this.addedAt = addedAt;
    }

    public Dvd(Integer id, String title, int year, String dvdCondition, 
    Long barCode, String description, String image, Timestamp addedAt, User user) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.dvdCondition = dvdCondition;
        this.barCode = barCode;
        this.description = description;
        this.image = image;
        this.addedAt = addedAt;
        this.user = user;
    }

    public Dvd() {
    }

    public Dvd(String title, int year, String dvdCondition, Long barCode, String description, String image,
            Timestamp addedAt, User user, Category category) {
        this.title = title;
        this.year = year;
        this.dvdCondition = dvdCondition;
        this.barCode = barCode;
        this.description = description;
        this.image = image;
        this.addedAt = addedAt;
        this.user = user;
        this.category = category;
    }
    
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getDvdCondition() {
        return dvdCondition;
    }

    public void setDvdCondition(String dvdCondition) {
        this.dvdCondition = dvdCondition;
    }

    public Long getBarCode() {
        return barCode;
    }

    public void setBarCode(Long barCode) {
        this.barCode = barCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Timestamp getAddedAt() {
        return addedAt;
    }

    public void setAddedAt(Timestamp addedAt) {
        this.addedAt = addedAt;
    }

    @Override
    public String toString() {
        return "Dvd [addedAt=" + addedAt + ", barCode=" + barCode + ", category=" + category + ", description="
                + description + ", dvdCondition=" + dvdCondition + ", id=" + id + ", image=" + image + ", title="
                + title + ", user=" + user + ", year=" + year + "]";
    }    
   

}
