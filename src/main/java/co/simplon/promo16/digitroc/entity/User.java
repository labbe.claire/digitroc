package co.simplon.promo16.digitroc.entity;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class User implements UserDetails {
    
    private Integer id;
    @NotBlank
    private String username;
    @Email
    @NotBlank
    private String email;
    @NotBlank
    @Size(min=8, max=45)
    private String password;
    private String firstname;
    private String lastname;
    private String city;
    private String role;
    private Timestamp createdAt;
    
    public User(Integer id, @NotBlank String username, @Email @NotBlank String email,
            @NotBlank @Size(min = 8, max = 45) String password, String firstname, String lastname, String city) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.city = city;
    }

    public User(@NotBlank String username, @Email @NotBlank String email,
            @NotBlank @Size(min = 8, max = 45) String password, String firstname, String lastname, String city) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.city = city;
    }

    public User() {
    }

    public User(@NotBlank String username, @Email @NotBlank String email,
            @NotBlank @Size(min = 8, max = 45) String password, String firstname, String lastname, String city,
            String role, Timestamp createdAt) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.city = city;
        this.role = role;
        this.createdAt = createdAt;
    }
    
    public User(Integer id, @NotBlank String username, @Email @NotBlank String email,
            @NotBlank @Size(min = 8, max = 45) String password, String firstname, String lastname, String city,
            String role, Timestamp createdAt) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.city = city;
        this.role = role;
        this.createdAt = createdAt;
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority("ROLE_USER"));
    }
    @Override
    public String getUsername() {
        return username;
    }
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    @Override
    public boolean isEnabled() {
        return true;
    }
    @Override
    public String toString() {
        return "User [city=" + city + ", createdAt=" + createdAt + ", firstname=" + firstname + ", lastname="
                + lastname + ", password=" + password + ", role=" + role + ", email=" + email + ", id="
                + id + ", username=" + username + "]";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }
    
    
    
}
