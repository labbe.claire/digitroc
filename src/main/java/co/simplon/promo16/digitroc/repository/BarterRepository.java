package co.simplon.promo16.digitroc.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.digitroc.entity.Barter;
import co.simplon.promo16.digitroc.entity.Dvd;
import co.simplon.promo16.digitroc.entity.User;

@Repository
public class BarterRepository {
    private static final String GET_BARTERS_LIST = "SELECT * FROM barter";
    private static final String GET_BARTER_BY_ID = "SELECT * FROM barter where id = ? ";
    private static final String SAVE_BARTER = "INSERT INTO barter ( message, demand_user_id, demanded_dvd_id) VALUES(?,?,?)";
    private static final String UPDATE_BARTER = "UPDATE barter SET status=?, selected_dvd_id=? WHERE id=?";
    private static final String DELETE_BARTER = "DELETE FROM barter WHERE id=?";

    @Autowired
    DataSource dataSource;

    @Autowired
    private UserRepository urepo;

    @Autowired
    private DvdRepository dvdrepo;

    List<Barter> bartersList;
    Connection cnx;

    public List<Barter> findAll() {
        bartersList = new ArrayList<>();
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(GET_BARTERS_LIST);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Barter barter = new Barter(
                        rs.getInt("id"),
                        rs.getBoolean("status"),
                        rs.getString("message"),
                        rs.getTimestamp("date"));

                User user = urepo.findById(rs.getInt("demand_user_id"));
                barter.setDemandUser(user);
                Dvd dvd1 = dvdrepo.findById(rs.getInt("demanded_dvd_id"));
                barter.setDemandedDvd(dvd1);

                Dvd dvd2 = dvdrepo.findById(rs.getInt("selected_dvd_id"));
                if (dvd2 != null) {
                    barter.setSelectedDvd(dvd2);
                }
                bartersList.add(barter);
            }
            return bartersList;
        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            // On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou
            // non
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return null;
    }

    public Barter findById(Integer id) {
        Barter barter = null;
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(GET_BARTER_BY_ID);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                barter = new Barter(
                        rs.getInt("id"),
                        rs.getBoolean("status"),
                        rs.getString("message"),
                        rs.getTimestamp("date"));

                User user = urepo.findById(rs.getInt("demand_user_id"));
                barter.setDemandUser(user);
                Dvd dvd1 = dvdrepo.findById(rs.getInt("demanded_dvd_id"));
                barter.setDemandedDvd(dvd1);

                Dvd dvd2 = dvdrepo.findById(rs.getInt("selected_dvd_id"));
                if (dvd2 != null) {
                    barter.setSelectedDvd(dvd2);
                }
            }

        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return barter;
    }

    public void save(Barter barter) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(SAVE_BARTER, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, barter.getMessage());
            ps.setInt(2, barter.getDemandUser().getId());
            ps.setInt(3, barter.getDemandedDvd().getId());

            if (ps.executeUpdate() == 1) {
                ResultSet result = ps.getGeneratedKeys();
                result.next();
                barter.setId(barter.getId());
            }

        } catch (SQLException e) {
            // e.getMessage() = pour mettre un message d'erreur
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }

    }

    public void update(Barter barter, Boolean status) {
        try {
            PreparedStatement ps = cnx.prepareStatement(UPDATE_BARTER);
            ps.setBoolean(1, barter.getStatus());
            ps.setInt(2, barter.getSelectedDvd().getId());

            ps.setInt(3, barter.getId());
            if (status == true) {

            }

        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }

    }

    public void deleteById(Integer id) {
        try {
            PreparedStatement ps = cnx.prepareStatement(DELETE_BARTER);
            ps.setInt(1, id);

        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }

    }

    public List<Barter> findByOwner(int idOwner) {
        List<Barter> list = new ArrayList<>();
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM barter WHERE demand_user_id=?");
            stmt.setInt(1, idOwner);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Barter barter = new Barter(
                        rs.getInt("id"),
                        rs.getBoolean("status"),
                        rs.getString("message"),
                        rs.getTimestamp("date"));

                User user = urepo.findById(rs.getInt("demand_user_id"));
                barter.setDemandUser(user);
                Dvd dvd1 = dvdrepo.findById(rs.getInt("demanded_dvd_id"));
                barter.setDemandedDvd(dvd1);

                Dvd dvd2 = dvdrepo.findById(rs.getInt("selected_dvd_id"));
                if (dvd2 != null) {
                    barter.setSelectedDvd(dvd2);
                }
                list.add(barter);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return list;
    }

    public List<Barter> findByDemand(int idDemand) {
        List<Barter> list = new ArrayList<>();
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM barter WHERE user_id=?");
            stmt.setInt(1, idDemand);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Barter barter = new Barter(
                        rs.getInt("id"),
                        rs.getBoolean("status"),
                        rs.getString("message"),
                        rs.getTimestamp("date"));

                User user = urepo.findById(rs.getInt("user_id"));
                barter.setDemandUser(user);
                Dvd dvd1 = dvdrepo.findById(rs.getInt("demanded_dvd_id"));
                barter.setDemandedDvd(dvd1);

                Dvd dvd2 = dvdrepo.findById(rs.getInt("selected_dvd_id"));
                if (dvd2 != null) {
                    barter.setSelectedDvd(dvd2);
                }
                list.add(barter);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return list;
    }

}
