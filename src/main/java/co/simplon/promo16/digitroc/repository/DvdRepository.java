package co.simplon.promo16.digitroc.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.digitroc.entity.Category;
import co.simplon.promo16.digitroc.entity.Dvd;
import co.simplon.promo16.digitroc.entity.User;

@Repository
public class DvdRepository {

    private static final String GET_DVD_LIST = "SELECT * FROM dvd";
    private static final String GET_DVD_BY_ID = "SELECT * FROM dvd where id = ? ";
    private static final String SAVE_DVD = "INSERT INTO dvd (title, year, dvd_condition, bar_code, description, image, user_id, category_id) VALUES(?,?,?,?,?,?,?,?)";
    private static final String UPDATE_DVD = "UPDATE dvd SET title=?, year=?, dvd_condition=?, bar_code=?,  description=?, image=? WHERE id = ?";
    private static final String DELETE_DVD = "DELETE FROM dvd WHERE id=?";
    private static final String GET_DVD_LIST_BY_KW = "SELECT * FROM dvd WHERE title LIKE ?";
    private static final String GET_DVD_BY_DATE = "SELECT * FROM dvd ORDER BY added_at DESC";

    @Autowired
    DataSource dataSource;

    @Autowired
    @Lazy
    private UserRepository urepo;

    @Autowired
    private CategoryRepository catrepo;

    Connection cnx;

    @Autowired
    DataSource datasource;

    /**
     * Méthode permettant d'afficher la liste de tous les dvd, en éditant toutes les 
     * caractéristiques qui le compose.
     */
        public List<Dvd> findAll() {
        List<Dvd> dvdList = new ArrayList<>();
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(GET_DVD_LIST);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Dvd dvd = new Dvd(
                        rs.getInt("id"),
                        rs.getString("title"),
                        rs.getInt("year"),
                        rs.getString("dvd_condition"),
                        rs.getLong("bar_code"),
                        rs.getString("description"),
                        rs.getString("image"),
                        rs.getTimestamp("added_at"));

                Category category = catrepo.findById(rs.getInt("category_id"));
                dvd.setCategory(category);

                User user = urepo.findById(rs.getInt("user_id"));
                dvd.setUser(user);

                dvdList.add(dvd);
            }
            return dvdList;
        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            // On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou
            // non
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return null;
    }

        public Dvd findById(Integer id) {
        Dvd dvd = null;
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(GET_DVD_BY_ID);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                dvd = new Dvd(
                        rs.getInt("id"),
                        rs.getString("title"),
                        rs.getInt("year"),
                        rs.getString("dvd_condition"),
                        rs.getLong("bar_code"),
                        rs.getString("description"),
                        rs.getString("image"),
                        rs.getTimestamp("added_at"));

                Category category = catrepo.findById(rs.getInt("category_id"));
                dvd.setCategory(category);

                User user = urepo.findById(rs.getInt("user_id"));
                dvd.setUser(user);
                return dvd;
            }

        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return dvd;
    }

        public void save(Dvd dvd) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(SAVE_DVD, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, dvd.getTitle());
            ps.setInt(2, dvd.getYear());
            ps.setString(3, dvd.getDvdCondition());
            ps.setLong(4, dvd.getBarCode());
            ps.setString(5, dvd.getDescription());
            ps.setString(6, dvd.getImage());
            ps.setInt(7, dvd.getUser().getId());
            ps.setInt(8, dvd.getCategory().getId());

            if (ps.executeUpdate() == 1) {
                ResultSet result = ps.getGeneratedKeys();
                result.next();
                dvd.setId(dvd.getId());
            }

        } catch (SQLException e) {
            // e.getMessage() = pour mettre un message d'erreur
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
    }

        public void update(Dvd dvd) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(UPDATE_DVD);
            ps.setString(1, dvd.getTitle());
            ps.setInt(2, dvd.getYear());
            ps.setString(3, dvd.getDvdCondition());
            ps.setLong(4, dvd.getBarCode());
            ps.setString(5, dvd.getDescription());
            ps.setString(6, dvd.getImage());

            ps.setInt(7, dvd.getId());

        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
    }

        public void deleteById(Dvd dvd) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(DELETE_DVD);
            ps.setInt(1, dvd.getId());

        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }

    }

        public List<Dvd> findByKeyword(String keyword) {
        List<Dvd> dvdList = new ArrayList<>();
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(GET_DVD_LIST_BY_KW);
            stmt.setString(1, "%" + keyword + "%");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Dvd dvd = new Dvd(
                        rs.getInt("id"),
                        rs.getString("title"),
                        rs.getInt("year"),
                        rs.getString("dvd_condition"),
                        rs.getLong("bar_code"),
                        rs.getString("description"),
                        rs.getString("image"),
                        rs.getTimestamp("added_at"));
                Category category = catrepo.findById(rs.getInt("category_id"));
                dvd.setCategory(category);
                User user = urepo.findById(rs.getInt("user_id"));
                dvd.setUser(user);
                dvdList.add(dvd);
            }
            return dvdList;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return null;
    }

    public List<Dvd> findByOwner(int idOwner) {
        List<Dvd> list = new ArrayList<>();
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM dvd WHERE user_id=?");
            stmt.setInt(1, idOwner);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                list.add(
                        new Dvd(
                                rs.getInt("id"),
                                rs.getString("title"),
                                rs.getInt("year"),
                                rs.getString("dvd_condition"),
                                rs.getLong("bar_code"),
                                rs.getString("description"),
                                rs.getString("image"),
                                rs.getTimestamp("added_at")));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return list;
    }

    public List<Dvd> findByDate() {
        List<Dvd> dvdList = new ArrayList<>();
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(GET_DVD_BY_DATE);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Dvd dvd = new Dvd(
                        rs.getInt("id"),
                        rs.getString("title"),
                        rs.getInt("year"),
                        rs.getString("dvd_condition"),
                        rs.getLong("bar_code"),
                        rs.getString("description"),
                        rs.getString("image"),
                        rs.getTimestamp("added_at"));

                Category category = catrepo.findById(rs.getInt("category_id"));
                dvd.setCategory(category);

                User user = urepo.findById(rs.getInt("user_id"));
                dvd.setUser(user);

                dvdList.add(dvd);
            }

            return dvdList;
        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return null;
    }

    public List<Dvd> findByTitle(String search) {
        List<Dvd> list = new ArrayList<>();
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
                    .prepareStatement("SELECT * FROM dvd WHERE LOWER(title) LIKE ?");
            stmt.setString(1, "%"+search.toLowerCase()+"%");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Dvd dvd = new Dvd(
                        rs.getInt("id"),
                        rs.getString("title"),
                        rs.getInt("year"),
                        rs.getString("dvd_condition"),
                        rs.getLong("bar_code"),
                        rs.getString("description"),
                        rs.getString("image"),
                        rs.getTimestamp("added_at"));

                Category category = catrepo.findById(rs.getInt("category_id"));
                dvd.setCategory(category);

                User user = urepo.findById(rs.getInt("user_id"));
                dvd.setUser(user);
                list.add(dvd);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return list;
    }


}
