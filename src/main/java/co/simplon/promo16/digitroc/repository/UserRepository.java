package co.simplon.promo16.digitroc.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.digitroc.entity.User;

@Repository
public class UserRepository implements UserDetailsService {
    private static final String GET_USERS_LIST = "SELECT * FROM user";
    private static final String GET_USER_BY_ID = "SELECT * FROM user where id = ? ";
    private static final String SAVE_USER = "INSERT INTO user (username, email, password, firstname, lastname, city) VALUES(?,?,?,?,?,?)";
    private static final String UPDATE_USER = "UPDATE user SET username=?, email=?, password=?, firstname=?, lastname=?, city=? WHERE id=?";
    private static final String DELETE_USER = "DELETE FROM user WHERE id=?";

    @Autowired
    DataSource dataSource;

    List<User> users;
    Connection cnx;

        public List<User> findAll() {
        users = new ArrayList<>();
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(GET_USERS_LIST);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                User user = new User(
                        rs.getInt("id"),
                        rs.getString("username"),
                        rs.getString("email"),
                        rs.getString("password"),
                        rs.getString("firstname"),
                        rs.getString("lastname"),
                        rs.getString("city"),
                        rs.getString("role"),
                        rs.getTimestamp("createdAt"));
                users.add(user);
            }
            cnx.close();
            return users;
        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            // On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou
            // non
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return null;
    }

        public User findById(int id) {
        User user = null;
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(GET_USER_BY_ID);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                user = new User(
                        rs.getInt("id"),
                        rs.getString("username"),
                        rs.getString("email"),
                        rs.getString("password"),
                        rs.getString("firstname"),
                        rs.getString("lastname"),
                        rs.getString("city"),
                        rs.getString("role"),
                        rs.getTimestamp("createdAt"));
            }

        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return user;
    }

        public User save(User user) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(SAVE_USER, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, user.getUsername());
            ps.setString(2, user.getEmail());
            ps.setString(3, user.getPassword());
            ps.setString(4, user.getFirstname());
            ps.setString(5, user.getLastname());
            ps.setString(6, user.getCity());
            if (ps.executeUpdate() == 1) {
                ResultSet result = ps.getGeneratedKeys();
                result.next();
                user.setId(result.getInt(1));
            }
            return user;
        } catch (SQLException e) {
            // e.getMessage() = pour mettre un message d'erreur
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return null;

    }

        public User update(User user) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(UPDATE_USER);
            ps.setString(1, user.getUsername());
            ps.setString(2, user.getEmail());
            ps.setString(3, user.getPassword());
            ps.setString(4, user.getFirstname());
            ps.setString(5, user.getLastname());
            ps.setString(6, user.getCity());

            ps.setInt(7, user.getId());
            return user;
        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return null;

    }

        public void deleteById(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(DELETE_USER);
            ps.setInt(1, id);

        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
    }

    public User findByEmail(String email) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement("SELECT * FROM user WHERE email=?");
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                User user = new User(
                    rs.getInt("id"),
                    rs.getString("username"),
                    rs.getString("email"),
                    rs.getString("password"),
                    rs.getString("firstname"),
                    rs.getString("lastname"),
                    rs.getString("city"),
                    rs.getString("role"),
                    rs.getTimestamp("createdAt"));
                return user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return null;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = findByEmail(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return user;
    }

}
