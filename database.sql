DROP TABLE IF EXISTS `barter`;
DROP TABLE IF EXISTS `dvd`;
DROP TABLE IF EXISTS `category`;
DROP TABLE IF EXISTS `user`;

CREATE TABLE user (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  username VARCHAR (50) NOT NULL UNIQUE,
  email VARCHAR (50) NOT NULL UNIQUE,
  password VARCHAR(100) NOT NULL,
  firstname VARCHAR (50) NOT NULL,
  lastname VARCHAR (50) NOT NULL,
  city VARCHAR(100),
  role VARCHAR(50),
  createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
) DEFAULT CHARSET UTF8;

CREATE TABLE category (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR (50) NOT NULL
) DEFAULT CHARSET UTF8;

CREATE TABLE dvd (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  title VARCHAR (50) NOT NULL,
  year INT (10),
  dvd_condition VARCHAR (255),
  bar_code BIGINT (15) NOT NULL,
  description VARCHAR (500),
  image VARCHAR (255),
  added_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  user_id INT,
  category_id INT,
  FOREIGN KEY (user_id) REFERENCES user (id),
  FOREIGN KEY (category_id) REFERENCES category (id)
) DEFAULT CHARSET UTF8;

CREATE TABLE `barter` (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  status BOOLEAN,
  message VARCHAR (255),
  date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  demand_user_id INT,
  demanded_dvd_id INT,
  selected_dvd_id INT,
  FOREIGN KEY (demand_user_id) REFERENCES user (id),
  FOREIGN KEY (demanded_dvd_id) REFERENCES dvd (id),
  FOREIGN KEY (selected_dvd_id) REFERENCES dvd (id)
) DEFAULT CHARSET UTF8;


INSERT INTO user (id, username, email, password, firstname, lastname, city, role) VALUES (1, 'Test', 'test@test.com', '$2a$10$mSgQaVa94oylqvxZdeaYWObksaiCoJ6MU2Xw3JeGud.lMX5vKJszG', 'test', 'Testy', 'Lyon', 'ROLE_USER');
INSERT INTO user (id, username, email, password, firstname, lastname, city, role) VALUES (2,'Test1', 'test1@test.com', '$2a$10$mSgQaVa94oylqvxZdeaYWObksaiCoJ6MU2Xw3JeGud.lMX5vKJszG', 'test', 'Testy', 'Lyon', 'ROLE_USER');
INSERT INTO user (id, username, email, password, firstname, lastname, city, role) VALUES (3, 'Test2', 'test2@test.com', '$2a$10$mSgQaVa94oylqvxZdeaYWObksaiCoJ6MU2Xw3JeGud.lMX5vKJszG', 'test', 'Testy', 'Lyon', 'ROLE_USER');
INSERT INTO user (id, username, email, password, firstname, lastname, city, role) VALUES (4, 'Test3', 'test3@test.com', '$2a$10$mSgQaVa94oylqvxZdeaYWObksaiCoJ6MU2Xw3JeGud.lMX5vKJszG', 'test', 'Testy', 'Lyon', 'ROLE_USER');
INSERT INTO user (id, username, email, password, firstname, lastname, city, role) VALUES (5, 'Test4', 'test4@test.com', '$2a$10$mSgQaVa94oylqvxZdeaYWObksaiCoJ6MU2Xw3JeGud.lMX5vKJszG', 'test', 'Testy', 'Lyon', 'ROLE_USER');
INSERT INTO user (id, username, email, password, firstname, lastname, city, role) VALUES (6, 'Test5', 'test5@test.com', '$2a$10$mSgQaVa94oylqvxZdeaYWObksaiCoJ6MU2Xw3JeGud.lMX5vKJszG', 'test', 'Testy', 'Lyon', 'ROLE_USER');
INSERT INTO user (id, username, email, password, firstname, lastname, city, role) VALUES (7, 'Test6', 'test6@test.com', '$2a$10$mSgQaVa94oylqvxZdeaYWObksaiCoJ6MU2Xw3JeGud.lMX5vKJszG', 'test', 'Testy', 'Lyon', 'ROLE_USER');
INSERT INTO user (id, username, email, password, firstname, lastname, city, role) VALUES (8, 'Test7', 'test7@test.com', '$2a$10$mSgQaVa94oylqvxZdeaYWObksaiCoJ6MU2Xw3JeGud.lMX5vKJszG', 'test', 'Testy', 'Lyon', 'ROLE_USER');
INSERT INTO user (id, username, email, password, firstname, lastname, city, role) VALUES (9, 'Test8', 'test8@test.com', '$2a$10$mSgQaVa94oylqvxZdeaYWObksaiCoJ6MU2Xw3JeGud.lMX5vKJszG', 'test', 'Testy', 'Lyon', 'ROLE_USER');
INSERT INTO user (id, username, email, password, firstname, lastname, city, role) VALUES (10, 'Test9', 'test9@test.com', '$2a$10$mSgQaVa94oylqvxZdeaYWObksaiCoJ6MU2Xw3JeGud.lMX5vKJszG', 'test', 'Testy', 'Lyon', 'ROLE_USER');
INSERT INTO user (id, username, email, password, firstname, lastname, city, role) VALUES (11, 'Test10', 'test10@test.com', '$2a$10$mSgQaVa94oylqvxZdeaYWObksaiCoJ6MU2Xw3JeGud.lMX5vKJszG', 'test', 'Testy', 'Lyon', 'ROLE_USER');

INSERT INTO category (id, name) VALUES (1, 'Drame');
INSERT INTO category (id, name) VALUES (2, 'Action');
INSERT INTO category (id, name) VALUES (3, 'Policier');
INSERT INTO category (id, name) VALUES (4, 'Thriller');
INSERT INTO category (id, name) VALUES (5, 'Romance');
INSERT INTO category (id, name) VALUES (6, 'Horreur');
INSERT INTO category (id, name) VALUES (7, 'Comédie musicale');
INSERT INTO category (id, name) VALUES (8, 'Science-fiction');
INSERT INTO category (id, name) VALUES (9, 'Fantasy');
INSERT INTO category (id, name) VALUES (10, 'Fantastique');
INSERT INTO category (id, name) VALUES (11, 'Aventures');
INSERT INTO category (id, name) VALUES (12, 'Historique');
INSERT INTO category (id, name) VALUES (13, 'Fantasy');
INSERT INTO category (id, name) VALUES (14, 'Comédie');
INSERT INTO category (id, name) VALUES (15, 'Animation');
INSERT INTO category (id, name) VALUES (16, 'Western');

INSERT INTO dvd (id, title, year, dvd_condition, bar_code, description, image, added_at, user_id, category_id) VALUES (1, 'Voyage à Tokyo', '1953', 'sous plastique', '1234567890987', "Un couple âgé entreprend un voyage pour rendre visite à ses enfants. D'abord accueillis avec les égards qui leur sont dus, les parents s'avèrent bientôt dérangeants. Seule Noriko, la veuve de leur fils mort à la guerre, semble réellement contente de les voir et trouve du temps à leur consacrer. Les enfants, quant à eux, se cotisent pour leur offrir un séjour dans la station thermale d'Atami, loin de Tokyo.", '/img/IMG_20220219_192710.jpg','2021-12-01', 1,1);

INSERT INTO dvd (id, title, year, dvd_condition, bar_code, description, image, added_at, user_id, category_id) VALUES (2, 'Arrietty', '2010', 'convenable', '1344567890987', "Dans la banlieue de Tokyo, sous le plancher d'une vieille maison perdue au coeur d'un immense jardin, la minuscule Arrietty vit en secret avec sa famille. Ce sont des Chapardeurs. Arrietty connaît les règles : on emprunte que ce dont on a besoin, en tellement petite quantité que les habitants de la maison ne s'en aperçoivent pas.", '/img/IMG_20220219_192716.jpg','2021-12-03',1,15);
INSERT INTO dvd (id, title, year, dvd_condition, bar_code, description, image, added_at, user_id, category_id) VALUES (3, 'Le parrain 3', '1990', 'état moyen', '1344567890987', "Atteignant la soixantaine, Michael Corleone désire à la fois renouer avec les siens et se réhabiliter aux yeux de la société, surtout de l'Église. Il arrivera presque à ses fins, mais sa vie passée et ses anciens ennemis le rattraperont plus vite. Michael Corleone est fatigué.", '/img/IMG_20220219_192804.jpg','2021-12-03',4,1);
INSERT INTO dvd (id, title, year, dvd_condition, bar_code, description, image, added_at, user_id, category_id) VALUES (4, 'Hair', '1979', 'bon état', '1234567890567', "Le jeune Claude Bukowski, appelé pour aller combattre au Viêtnam, quitte son Oklahoma natal pour rejoindre New York, où il doit se présenter aux autorités militaires.", '/img/IMG_20220219_192721.jpg','2021-12-04',3,7);
INSERT INTO dvd (id, title, year, dvd_condition, bar_code, description, image, added_at, user_id, category_id) VALUES (5, "Il était une fois dans l'ouest", '1969', 'en très bon état', '1243567890987', "Alors qu'il prépare une fête pour sa femme, Bet McBain est tué avec ses trois enfants. Jill McBain hérite alors les terres de son mari, terres que convoite Morton, le commanditaire du crime (celles-ci ont de la valeur maintenant que le chemin de fer doit y passer).", '/img/IMG_20220219_192728.jpg','2021-12-05',1,16);
INSERT INTO dvd (id, title, year, dvd_condition, bar_code, description, image, added_at, user_id, category_id) VALUES (6, 'JCVD', '2008', 'bon état', '1234569990987', "Jean-Claude Van Damme vient chercher dans son pays d'enfance le calme et le repos qu'il ne trouve plus aux États-Unis.", '/img/IMG_20220219_192736.jpg','2021-12-05',1,2);
INSERT INTO dvd (id, title, year, dvd_condition, bar_code, description, image, added_at, user_id, category_id) VALUES (7, 'Amour et Amnésie', '2004', 'bon état', '1234567790987', "Lui, c'est le dragueur aux relations sans lendemain. Elle, c'est une charmante habitante du coin, qui souffre d'une étrange maladie. Pourtant lorsque Henry rencontre Lucy, il est immédiatement sous le charme. Mais chaque nuit, Lucy oublie tout ce qu'elle a vécu la journée précédente. Pour être à ses côtés, Henry devra apprendre à la séduire à nouveau, chaque jour étant comme le premier.", '/img/IMG_20220219_192814.jpg','2021-12-06',2,14);
INSERT INTO dvd (id, title, year, dvd_condition, bar_code, description, image, added_at, user_id, category_id) VALUES (8, "Songe d'une nuit d'été", '1999', 'état ok', '1234567890987', "Le duc Thésée s'apprête à épouser Hippolyta quand il reçoit la visite d'Égée. Celui-ci se plaint que sa fille Hermia, éprise de Lysandre, refuse Démétrius, l'époux qu'il lui destinait. La jeune fille s'enfuit alors dans un bois avec celui qu'elle aime, et s'y endort avec lui. Ils sont bientôt rejoints par Helena, amoureuse, elle, de Démétrius, ainsi que par Oberon et Titania, le roi et la reine des fées, qui se querellent.", '/img/IMG_20220219_192833.jpg','2021-12-07',2,13);
INSERT INTO dvd (id, title, year, dvd_condition, bar_code, description, image, added_at, user_id, category_id) VALUES (9, 'Akira', '1988', 'état correct', '8734567890987', "Tetsuo, un adolescent ayant vécu une enfance difficile, est la victime d'expériences visant à développer les capacités psychiques qui dorment en chacun de nous. Ainsi doté d'une puissance que lui meme ne peut imaginer, Tetsuo décide de partir en guerre contre le monde qui l'a opprimé. Dès lors, Il se retrouve au coeur d'une légende populaire qui annonce le retour prochain d'Akira, un enfant aux pouvoirs extra-ordinaires censé délivrer Tokyo du chaos...", '/img/IMG_20220219_192845.jpg','2021-12-08',1,8);
INSERT INTO dvd (id, title, year, dvd_condition, bar_code, description, image, added_at, user_id, category_id) VALUES (10, 'La haine', '1995', 'état moyen', '1234567897887', "Une émeute éclate dans la cité des Muguets, à la suite d'une bavure policière. Au petit matin, trois copains se retrouvent.", '/img/IMG_20220219_192902.jpg','2021-12-09',1,1);




INSERT INTO barter (id, status, message, date, demand_user_id, demanded_dvd_id,selected_dvd_id) VALUES (1, true, 'Echange en attente', '2022-01-02',1, 2, 3);
INSERT INTO barter (id, status, message, date, demand_user_id, demanded_dvd_id,selected_dvd_id) VALUES (2, true, 'Echange en attente', '2022-01-03',1, 4, 9);
INSERT INTO barter (id, status, message, date, demand_user_id, demanded_dvd_id,selected_dvd_id) VALUES (3, true, 'Echange en attente', '2022-01-03',1, 8, 10);